$("#vanilla_tor").tabulator({
  height:"100%",
  layout:"fitData",
  columns:[
    {title:"Country", field:"country"},
    {title:"# Measurements", field:"total_count", align:"center"},
    {title:"Success Percent", field:"success_percent", align:"center", sorter:"number", formatter:function(cell, formatterParams){
      var value = cell.getValue();
      if(value < 50) {
        return "<span style='color:red; font-weight:bold;'>" + value + "</span>";
      } else{
        return value;
      }
    }},
    {title:"# Successes", field:"success_count", align:"center"},
    {title:"# Failures", field:"failure_count", align:"center"},
  ],
  initialSort:[
          {column:"success_percent", dir:"asc"},
      ]
});

$.getJSON("countries.json")
.done(function( json ) {
  $("#vanilla_tor").tabulator("setData", json);
  showBlocking();
})

$("#filter").click(showBlocking);
$("#filter-clear").click(showAll);

function showBlocking() {
  $("#vanilla_tor").tabulator("setFilter", "success_percent", "<", 50);
  $("#filter").hide();
  $("#filter-clear").show();
}

function showAll() {
  $("#vanilla_tor").tabulator("clearFilter");
  $("#filter").show();
  $("#filter-clear").hide();
}